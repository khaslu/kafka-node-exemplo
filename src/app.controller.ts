import { Controller, Get, OnModuleInit, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { Client, ClientKafka, Ctx, KafkaContext, MessagePattern, Payload, Transport } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

interface Dados {
  chave: string;
  valor: string;
}

const topico = 'topico.teste';

@Controller()
export class AppController implements OnModuleInit {
  @Client({
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: topico,
        brokers: ['18.229.16.160:9092'],
      },
      consumer: {
        groupId: topico
      }
    }
  })
  private client: ClientKafka;

  constructor(private readonly appService: AppService) { }

  async onModuleInit() {
    this.client.subscribeToResponseOf(topico);
    await this.client.connect();
  }

  @MessagePattern(topico)
  topicoTesteValores(@Payload() message: Dados, @Ctx() context: KafkaContext) {
    console.log(`Message: ${JSON.stringify(message)}`);
    console.log(`Topic: ${context.getTopic()}`);
  }

  @Get(':valor')
  getHello(@Param() params) {
    firstValueFrom(this.client.send(topico, {
      chave: `Teste Integração ${new Date().toISOString()}`,
      valor: params?.valor
    } as Dados));

    return this.appService.getHello();
  }
}
