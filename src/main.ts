import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const [app, microservice] = await Promise.all([
    NestFactory.create(AppModule),
    NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
      transport: Transport.KAFKA,
      options: {
        client: {
          brokers: ['18.229.16.160:9092'],
          clientId: 'nodejs-exemplo',
        }
      }
    })
  ]);

  await Promise.all([
    microservice.listen(),
    app.listen(3000)
  ])
}
bootstrap();
